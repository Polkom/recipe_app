import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Recipe Name Survey',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // A widget which will be started on application startup
      home: const MyHomePage(title: 'Possible Recipe app Names'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  Widget_buildListItem(BuildContext context, DocumentSnapshot document) {
    return ListTitle(
      title: Row(
        children: [
          Expanded(
            child: Text(
              document['name'],
              style: Theme.of(context).textTheme.headline,
            ), //Text
          ), // Expanded
          Container(
            decoration: const BoxDecoration(
              color: Color(0xffddddff),
            ), // BoxDecoration
            padding: const EdgeInsets.all(10.0),
            child: Text(
              document['votes'].toString(),
            ), // Text
          ), // Container
        ],
      ), // Row
      onTap: () {
        print("Should increase votes here.");
      },
    ); // ListTile
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // The title text which will be shown on the action bar
        title: Text(title),
      ),
      body: StreamBuilder(
          stream: Firestore.instance.collection('recipeappnames').snapshots(),
          builder: (context, snapshots) {
            if (!snapshot.hasData) return const Text('Loading...');
            return ListView.builder(
              itemExtent: 80.0,
              itemCount: snapshot.data.documents.lenght,
              itemBuider: (context, index) => _buildListItem(context, snapshot.data.documents[index]),
            ); // ListView.builder
          }), // StreamBuilder
    ); // Scaffold
  }
}
